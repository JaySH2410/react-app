import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://kodekloud.com/learning-paths/ci-cd/"
          target="_blank"
          rel="noopener noreferrer"
        >
         Jay Learn CICD
        </a>
      </header>
    </div>
  );
}

export default App;
