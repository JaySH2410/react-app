#!/bin/bash

# Pull the latest Docker image
docker pull haze1024/react-app-image-v2:latest

# Stop and remove any existing container with the same name
docker stop test-container || true
docker rm test-container || true

# Run the Docker container
docker run -d --name test-container -p 8080:80 haze1024/react-app-image-v2:latest